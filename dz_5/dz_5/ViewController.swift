//
//  ViewController.swift
//  dz_5
//
//  Created by Andrey Bakanov on 13.03.2019.
//  Copyright © 2019 Andrey Bakanov. All rights reserved.
//

import UIKit
  import Foundation

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //countName()
        //testEndName(name: "Vladimirovich")
        //razbitStroku(name: "AndreyBakanov")
        //strokaNaoborot(text: "Ось" )
        //insertElement()
        //find()
        //charSetCheck()
       // sortArray ()
        translit()
    }
    
    /////////////////////////////////////
    //dz1 podschet kol-va simvolov v slove
    func countName(){
        let name = "Andrii"
        print("Name has \(name.count) symbols")
    }
    
    ////////////////////////////////////////
    //dz2 proverka slova na suffix "ich" ili "na"
    func testEndName(name: String) {
        if name.hasSuffix("ich") {
            print("word has suffix - ich")
        }
        else  if name.hasSuffix("na"){
         print("word has suffix - na")}
        }
    
    ////////////////////////////////////////
    //dz3 razbit stroku na 2 chasti
    func razbitStroku(name: String){
        let endSent = name.firstIndex(of: "y")!
        let str1 = name [...endSent]
        
        let startSent = name.firstIndex(of: "B")!
        let str2 = name [startSent...]
        let str3 = str1 + " " + str2
        print("\(str3)")
        
    }
    
    //////////////////////////////////////
    //dz4 stroka naoborot
    func strokaNaoborot(text: String){
        let firstLetter = String (text[text.startIndex])
        let secondLetter = String(text[text.index(after: text.startIndex)])
        let endLetter = String(text[text.index(before: text.endIndex)])
        let textNooborot = endLetter + secondLetter + firstLetter
        print ("\(textNooborot)")
    }
    ///////////////////////////////////////
    //dz5 vstavka zapatoi
    func insertElement(){
        var number = "1234567"
        let count = number.count
        if count <= 6 {
            number.insert(",", at: number.index(number.endIndex, offsetBy: -3))}
        else if count >= 6 {
            number.insert(",", at: number.index(number.endIndex, offsetBy: -3))
            number.insert(",", at: number.index(number.endIndex, offsetBy: -7))}
        print(number)
    }

    ///////////////////////////////////////
    //dz6 Proverka parola
    func charSetCheck() {
        let charSet = CharacterSet.decimalDigits
        let loverCase = CharacterSet.lowercaseLetters
        let upperCase = CharacterSet.uppercaseLetters
        let symbols = CharacterSet.punctuationCharacters
        var rangeOfCharacterTypesCount:Int = 0
        var rangeOfCharacterTypes:String = ""
        let pass = "ddddd5F!"
        
        if pass.rangeOfCharacter(from: charSet) != nil {
            rangeOfCharacterTypesCount += 1
            rangeOfCharacterTypes = rangeOfCharacterTypes + "a"
                    }
        
        if pass.rangeOfCharacter(from: loverCase) != nil{
            rangeOfCharacterTypesCount += 1
            rangeOfCharacterTypes = rangeOfCharacterTypes + "b"
            
        }
        if pass.rangeOfCharacter(from: upperCase) != nil{
            rangeOfCharacterTypesCount += 1
            rangeOfCharacterTypes = rangeOfCharacterTypes + "c"
            
        }
        if pass.rangeOfCharacter(from: symbols) != nil{
            rangeOfCharacterTypesCount += 1
            rangeOfCharacterTypes = rangeOfCharacterTypes + "d"
            
        }
        if pass.rangeOfCharacter(from: symbols) != nil && pass.rangeOfCharacter(from: charSet) != nil && pass.rangeOfCharacter(from: loverCase) != nil && pass.rangeOfCharacter(from: upperCase) != nil {
            rangeOfCharacterTypesCount += 1
            rangeOfCharacterTypes = rangeOfCharacterTypes + "e"
            
        }
        
        print("\(rangeOfCharacterTypesCount)\(rangeOfCharacterTypes)")
        
    }
    
    ///////////////////////////////////////
    //dz7 sort array
    
    func sortArray() {
       var array = [9,1,2,5,1,7]
        let size = array.count
        
        for i in 0..<size {
            let pass  = (size - 1) - i
            //perestavlaem i sravnivaem
            for j in 0..<pass {
                let key = array[j]
                if key > array[j + 1] {
                    let temp = array[j + 1]
                    array[j + 1] = key
                    array[j] = temp
                }
            }
        }
        //udalenie dublikata
        let size1 = size - 1
        for i in 0..<size1 {
            if array[0] == array [0+i]{
                array.remove(at: 0+i)
            }
        }
        
      for item in array {
          print(item)
       }
    }
    
    
    ////////////////////////
    //dz8 perevod v trs]anslit
    func translit(){
        
//        let dict = ["b": "б", "z": "з", "y":"я"]
//               let string = "bgg"
//               let firstLetterString = string[string.startIndex]
//
//        if let entry = dict.first(where: { ( key, _) in key.contains(firstLetterString) }) {
//            print(entry.key)
//        } else {
//            print("no match")
//        }

        //import Foundation
        
        let mutableString = NSMutableString(string: "привет")
        
        CFStringTransform(mutableString, nil, kCFStringTransformStripDiacritics, false)
        print(mutableString) 
    }
           
}


